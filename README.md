### Character Counter

This is a simple multithreaded char counter that accept words from
command line arguments. It creates argc - 1 threads, for each words, and 
writes to intermediate file in the following format:

`a 1`

`a 1`

`d 1`

`z 1`

`d 1`

The file name is cc-x, where x is thread number.

It then creates argc - 1 thread to read the files and inserts into pair
vector so that the characters can be sorted and grouped next to each other.
The characters in sorted vector are push into a map<char,vector<string>>
for example:

`z 1`

`d 1 1`

`a 1 1`


Then it calculates the characters values.

### Compile
Comile using `g++ -o cc charCounter.cpp -pthread`

 

#include <algorithm>
#include <cctype>
#include <cstdio>
#include <fstream>
#include <iostream>
#include <unordered_map>
#include <string>
#include <thread>
#include <vector>
#include <sstream>
#include <iterator>
#include <mutex>
#include <numeric>
using namespace std;

mutex m;

struct Pair {
	char ch;
	int i;
	Pair() {}
	Pair(char ch, int i) : ch(ch),i(i) {}
};

string genFileName(int i) {
	stringstream ss;
	ss << "cc-" << i;
	return ss.str();
}

void writePairsToFile(const string word,int i) {
	
	string fileName = genFileName(i);
	fstream out(fileName,fstream::out | fstream::trunc);

	for(char c : word) {
		if ( (!isspace(c)) || (!ispunct(c))) {
			out << c << " " << "1" << endl;
		}
	}	
	out.close();
}

void readTempFiles(vector<Pair> &p,int i) {
	
	string fileName = genFileName(i);
	fstream input(fileName);
	vector<Pair> tempPair;
	Pair inputPair;
	while(input >> inputPair.ch >> inputPair.i) {
		tempPair.push_back(inputPair);
	}
	//since the out file is a shared object, only one thread must access it at a time
	m.lock();
	copy(tempPair.begin(),tempPair.end(),back_inserter(p));
	m.unlock();
}

void calculate(string outFile,unordered_map<char, vector<int>> &val) {
	fstream out(outFile, fstream::out | fstream::trunc);
	for(auto v : val) {
		int total = accumulate(v.second.begin(),v.second.end(),0.0);
		out << v.first << " " << total << endl;
	}
}

int main(int argc, char** argv ) {
	if(argc < 2 && argc < 16) {
		cout << "Usage: ./charCount args1, ...\n";
		return 1;
	}
	vector<thread> ths;
	//creates argc thread, max 16, and writes char and 1 to intermediate file 
	//in cc-x format where x is i index
	for(int i = 1;i < argc;i++) {
		ths.push_back(thread(&writePairsToFile,argv[i],i));
	}
	//wait for all argc thread to complete their tasks
	for(auto &th : ths)
		th.join();

	ths.clear();
	//create argc threads and read to pairs from temp files to pair vector
	vector<Pair> p;
	for(int i = 1;i < argc;i++) {
		ths.push_back(thread(&readTempFiles,ref(p),i));
	}
	for(auto &th : ths)
		th.join();
	//sort intermedate temp pairs so that their occurrence can be counted in line92
	//for example suppose there is c 1,a, 1, a1 in the temp file this will sort it in
	//ascending order a 1, a1, c1
	sort(p.begin(),p.end(),[] (Pair &p1,Pair &p2) {
		return p1.ch < p2.ch;
	});

	unordered_map<char, vector<int>> values; 
	int i = 0;
	while(i < p.size()) {
		int j = i + 1;
		while (j < p.size() && p[j].ch == p[i].ch)
			j++;
		for (int k = i; k < j;++k) {
			values[p[k].ch].push_back(1);
		}
		i = j;
	}
	ths.clear();
	//finally calculate the character the occurrence count
	//for example suppose a 1, a 1, c 1 in the values map, then the result of this
	//func is c1, a2
	for(int i = 0; i < values.size();++i)
		ths.push_back(thread(&calculate,"out",ref(values)));
	for(auto &t : ths)
		t.join();
}

